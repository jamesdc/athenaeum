<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>LibraryGridView</name>
    <message>
        <location filename="../LibraryGridView.qml" line="24"/>
        <source>Search %L1 Games...</source>
        <translation>%L1 Oyunda Ara...</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="82"/>
        <source>All (%L1)</source>
        <translation>Tümü (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="83"/>
        <source>Installed (%L1)</source>
        <translation>Kurulu (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="84"/>
        <source>Recent (%L1)</source>
        <translation>Yeni (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="85"/>
        <source>Has Updates (%L1)</source>
        <translation>Güncelleme Var (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="86"/>
        <source>Processing (%L1)</source>
        <translation>İşleniyor (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="167"/>
        <source>Play</source>
        <translation>Oyna</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="176"/>
        <source>Install</source>
        <translation>Kur</translation>
    </message>
    <message>
        <source>View In Store</source>
        <translation type="vanished">Mağazada Görüntüle</translation>
    </message>
</context>
<context>
    <name>LibraryListView</name>
    <message>
        <location filename="../LibraryListView.qml" line="24"/>
        <source>Search %L1 Games...</source>
        <translation>%L1 Oyunda Ara...</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="108"/>
        <source>All (%L1)</source>
        <translation>Tümü (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="109"/>
        <source>Installed (%L1)</source>
        <translation>Kurulu (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="110"/>
        <source>Recent (%L1)</source>
        <translation>Yeni (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="111"/>
        <source>Has Updates (%L1)</source>
        <translation>Güncelleme Var (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="112"/>
        <source>Processing (%L1)</source>
        <translation>İşleniyor (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="299"/>
        <source>New</source>
        <translation>Yeni</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="319"/>
        <source>Nothing seems to be here.</source>
        <translation>Burada hiçbir şey yok gibi görünüyor.</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="424"/>
        <source>Install</source>
        <translation>Kur</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="439"/>
        <source>Download Size:	%1</source>
        <translation>İndirme Boyutu:	%1</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="446"/>
        <source>Installed Size:	%1</source>
        <translation>Kurulu Boyutu:	%1</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="453"/>
        <source>Install Game?</source>
        <translation>Oyun Kurulsun Mu?</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="484"/>
        <source>Play</source>
        <translation>Oyna</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="484"/>
        <source>In-Game</source>
        <translation>Oyunda</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="504"/>
        <source>Update</source>
        <translation>Güncelle</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="507"/>
        <source>Uninstall</source>
        <translation>Kaldır</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="528"/>
        <source>Are you sure?</source>
        <translation>Emin misiniz?</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="778"/>
        <source>Description</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="783"/>
        <source>No description available.</source>
        <translation>Açıklama yok.</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="804"/>
        <source>Similar Games</source>
        <translation>Benzer Oyunlar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="463"/>
        <location filename="../LibraryListView.qml" line="538"/>
        <source>Yes</source>
        <translation>Evet</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="151"/>
        <source>Action</source>
        <translation>Aksiyon</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="152"/>
        <source>Adventure</source>
        <translation>Macera</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="153"/>
        <source>Arcade</source>
        <translation>Atari</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="154"/>
        <source>Board</source>
        <translation>Tahta</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="155"/>
        <source>Blocks</source>
        <translation>Bloklar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="156"/>
        <source>Card</source>
        <translation>Kart</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="157"/>
        <source>Kids</source>
        <translation>Çocuklar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="158"/>
        <source>Logic</source>
        <translation>Mantık</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="159"/>
        <source>RolePlaying</source>
        <translation>RolYapma</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="160"/>
        <source>Shooter</source>
        <translation>AteşEtme</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="161"/>
        <source>Simulation</source>
        <translation>Simülasyon</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="162"/>
        <source>Sports</source>
        <translation>Spor</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="163"/>
        <source>Strategy</source>
        <translation>Strateji</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="180"/>
        <source>Apply</source>
        <translation>Uygula</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="189"/>
        <source>Clear</source>
        <translation>Temizle</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="466"/>
        <location filename="../LibraryListView.qml" line="541"/>
        <location filename="../LibraryListView.qml" line="556"/>
        <location filename="../LibraryListView.qml" line="610"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="495"/>
        <source>Servers</source>
        <translation>Sunucular</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="577"/>
        <source>Resolve Error</source>
        <translation>Hatayı Çöz</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="589"/>
        <source>Clear error</source>
        <translation>Hatayı temizle</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="594"/>
        <source>Mark as installed</source>
        <translation>Kuruldu olarak işaretle</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="601"/>
        <source>Mark as uninstalled</source>
        <translation>Kaldırıldı olarak işaretle</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="832"/>
        <source>Reviews</source>
        <translation>İncelemeler</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="837"/>
        <source>This game has no reviews.</source>
        <translation>Bu oyun için hiç inceleme yok.</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1008"/>
        <source>Releases</source>
        <translation>Yayınlar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1013"/>
        <source>No release information available.</source>
        <translation>Yayın bilgisi yok.</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1036"/>
        <source>Version %1</source>
        <translation>Sürüm %1</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1051"/>
        <source>No release description available.</source>
        <translation>Yayın açıklaması yok.</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1068"/>
        <source>Anti-Features</source>
        <translation>Karşıt-Özellikler</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1081"/>
        <source>This game requires NonFree assets.</source>
        <translation>Bu oyun özgür olmayan varlıklar gerektiriyor.</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1083"/>
        <source>This game requires NonFree network services.</source>
        <translation>Bu oyun özgür olmayan ağ hizmetleri gerektiriyor.</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1096"/>
        <source>Developer</source>
        <translation>Geliştirici</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1110"/>
        <source>License</source>
        <translation>Lisans</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1139"/>
        <source>Links</source>
        <translation>Bağlantılar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1169"/>
        <source>Homepage</source>
        <translation>Ana Sayfa</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1171"/>
        <source>Bug Tracker</source>
        <translation>Hata İzleyici</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1173"/>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1175"/>
        <source>FAQ</source>
        <translation>SSS</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1178"/>
        <source>Donate</source>
        <translation>Bağış Yap</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1180"/>
        <source>Translation</source>
        <translation>Çeviri</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1182"/>
        <source>Unknown</source>
        <translation>Bilinmeyen</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1184"/>
        <source>Manifest</source>
        <translation>Bildirim</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1186"/>
        <source>Contact</source>
        <translation>İletişim</translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <location filename="../NavigationBar.qml" line="18"/>
        <source>Library</source>
        <translation>Kütüphane</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="29"/>
        <source>Servers</source>
        <translation>Sunucular</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="40"/>
        <source>Settings</source>
        <translation>Ayarlar</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="61"/>
        <source>Show games in a list view.</source>
        <translation>Oyunları liste görünümünde göster.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="71"/>
        <source>Show games in a grid view.</source>
        <translation>Oyunları ızgara görünümünde göster.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="80"/>
        <source>Reset settings to defaults.</source>
        <translation>Ayarları öntanımlı değerlere sıfırla.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="92"/>
        <source>Check For Updates</source>
        <translation>Güncellemeleri Denetle</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="96"/>
        <source>Update All</source>
        <translation>Tümünü Güncelle</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="100"/>
        <source>Exit</source>
        <translation>Çıkış</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="115"/>
        <source>You have operations pending.</source>
        <translation>Bekleyen işlemleriniz var.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="121"/>
        <source>Close Anyway</source>
        <translation>Yine de Kapat</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="127"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
</context>
<context>
    <name>ServersView</name>
    <message>
        <location filename="../ServersView.qml" line="66"/>
        <location filename="../ServersView.qml" line="72"/>
        <location filename="../ServersView.qml" line="89"/>
        <location filename="../ServersView.qml" line="95"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../ServersView.qml" line="174"/>
        <source>All</source>
        <translation>Tümü</translation>
    </message>
    <message>
        <location filename="../ServersView.qml" line="182"/>
        <source>Show empty</source>
        <translation>Boş göster</translation>
    </message>
    <message>
        <location filename="../ServersView.qml" line="190"/>
        <source>%L1 servers found.</source>
        <translation>%L1 sunucu bulundu.</translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <location filename="../SettingsView.qml" line="28"/>
        <source>Always Show Logs</source>
        <translation>Günlükleri Her Zaman Göster</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="35"/>
        <source>Notifications Enabled</source>
        <translation>Bildirimler Etkinleştirildi</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="46"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="69"/>
        <source>Reset Database</source>
        <translation>Veri Tabanını Sıfırla</translation>
    </message>
</context>
</TS>
